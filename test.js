var pull = require('pull-stream');
var test = require('tape');
var rendererToDuplex = require('./renderer');
var workerToDuplex = require('./worker');
var mock = require('./mock');

test('renderer duplex works as source pull-stream', function (t) {
  t.plan(2);
  var sourceChannel = mock.source('pull-electron-ipc-renderer');

  var clientStream = rendererToDuplex(sourceChannel);
  pull(
    clientStream,
    pull.collect(function (err, arr) {
      t.error(err, 'no error happened');
      t.deepEqual(arr, [10, 20, 30], 'data is [10,20,30]');
      t.end();
    }),
  );
});

test('renderer duplex works as a sink pull-stream', function (t) {
  t.plan(8);
  var expected = [
    {type: 'data', format: 'json', data: 10, seq: 1},
    {type: 'data', format: 'json', data: 20, seq: 2},
    {type: 'data', format: 'json', data: 30, seq: 3},
    {type: 'end', seq: 4},
  ];
  var sinkChannel = mock.sink(function (channel, x) {
    t.equal(channel, 'pull-electron-ipc-main', 'target channel is ipcMain');
    const e = expected.shift();
    t.deepEqual(x, e, 'data we are sending is ' + e.data);
  });

  var clientStream = rendererToDuplex(sinkChannel);
  pull(pull.values([10, 20, 30]), clientStream);
});

test('renderer duplex works as a transform pull-stream', function (t) {
  t.plan(2);
  var sinkChannel = mock.transform('pull-electron-ipc-renderer');

  var clientStream = rendererToDuplex(sinkChannel);
  pull(
    pull.values([1, 2, 3]),
    clientStream,
    pull.collect(function (err, arr) {
      t.error(err, 'no error happened');
      t.deepEqual(arr, [5, 10, 15], 'data is [5,10,15]');
      t.end();
    }),
  );
});

test('renderer duplex is resilient to re-ordering accidents', function (t) {
  t.plan(2);
  var sourceChannel = mock.reorderedSource('pull-electron-ipc-renderer');

  var clientStream = rendererToDuplex(sourceChannel);
  pull(
    clientStream,
    pull.collect(function (err, arr) {
      t.error(err, 'no error happened');
      t.deepEqual(arr, [10, 20, 30], 'data is [10,20,30]');
      t.end();
    }),
  );
});

test('worker duplex works as source pull-stream', function (t) {
  t.plan(2);
  var sourceChannel = mock.port();

  var clientStream = workerToDuplex(sourceChannel);
  pull(
    clientStream,
    pull.collect(function (err, arr) {
      t.error(err, 'no error happened');
      t.deepEqual(arr, [10, 20, 30], 'data is [10,20,30]');
      t.end();
    }),
  );
});

test.skip('FIXME: main duplex works as a sink pull-stream', function (t) {
  t.plan(8);
  var expected = [
    {type: 'data', format: 'json', data: 10, seq: 1},
    {type: 'data', format: 'json', data: 20, seq: 2},
    {type: 'data', format: 'json', data: 30, seq: 3},
    {type: 'end', seq: 4},
  ];
  var sinkChannel = mock.sink(function (channel, x) {
    t.equal(
      channel,
      'pull-electron-ipc-renderer',
      'target channel is ipcRenderer',
    );
    const e = expected.shift();
    t.deepEqual(x, e, 'data we are sending is ' + e.data);
  });

  var clientStream = workerToDuplex(sinkChannel, sinkChannel);
  pull(pull.values([10, 20, 30]), clientStream);
});

test.skip('FIXME: main duplex works as a transform pull-stream', function (t) {
  t.plan(2);
  var sinkChannel = mock.transform('pull-electron-ipc-main');

  var clientStream = workerToDuplex(sinkChannel, sinkChannel);
  pull(
    pull.values([1, 2, 3]),
    clientStream,
    pull.collect(function (err, arr) {
      t.error(err, 'no error happened');
      t.deepEqual(arr, [5, 10, 15], 'data is [5,10,15]');
      t.end();
    }),
  );
});

test('main duplex is resilient to re-ordering accidents', function (t) {
  t.plan(2);
  var sourceChannel = mock.reorderedPort();

  var clientStream = workerToDuplex(sourceChannel, sourceChannel);
  pull(
    clientStream,
    pull.collect(function (err, arr) {
      t.error(err, 'no error happened');
      t.deepEqual(arr, [10, 20, 30], 'data is [10,20,30]');
      t.end();
    }),
  );
});
