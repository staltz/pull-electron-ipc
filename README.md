# pull-electron-ipc

_Convert [Electron's IPC streams](https://electronjs.org/docs/api/ipc-main) (Main or Renderer) to duplex pull-streams_

```
npm install --save pull-electron-ipc
```

## Usage

The main thread in Electron must not do heavy processing, so we're going to use it only as a lightweight bridge between the frontend and the backend. The "frontend" is a renderer thread where you have a browser window with the DOM. The "backend" is a Node.js `worker_threads` instance, i.e. Worker. `pull-electron-ipc` only supports this architecture shape.

```mermaid
flowchart TB;
  Main <--> Worker
  Main <--> Renderer
```

To setup pull-electron-ipc in each of these three parts, do the following:

### Main thread

- Import this package as `require('pull-electron-ipc/main')`
- Give it **three** arguments:
  - `worker`
  - `ipcMain`
  - `webContents` (of the window you want to communicate with)

```js
const {Worker} = require('worker_threads')
const {ipcMain, app, BrowserWindow} = require('electron')
const setup = require('pull-electron-ipc/main')

app.on('ready', () => {
  const win = new BrowserWindow({ width: 800, height: 600 })
  win.loadURL(`file://${__dirname}/index.html`)

  const worker = new Worker(SOME_KIND_OF_FILENAME);

  setup(worker, ipcMain, win.webContents);
})
```

### Worker thread

- Import this package as `require('pull-electron-ipc/worker')`
- Give it **one** arguments:
  - `parentPort`

```js
const {parentPort} = require('worker_threads')
const toDuplex = require('pull-electron-ipc/worker')

const stream = toDuplex(parentPort)

pull(
  pull.values([20, 40, 60, 80]),
  stream,
  pull.drain(x => {
    console.log(x) // 2
                   // 4
                   // 6
                   // 8
  })
)
```

### Renderer thread

- Import this package as `require('pull-electron-ipc/renderer')`
- Give it **one** argument:
  - `ipcRenderer`

```js
const {ipcRenderer} = require('electron')
const pull = require('pull');
const toDuplex = require('pull-electron-ipc/renderer')

const stream = toDuplex(ipcRenderer);

pull(
  stream,
  pull.map(x => x * 0.1),
  stream
);
```
