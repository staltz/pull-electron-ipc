var impl = require('./implementation');

module.exports = function toDuplex(ipcRenderer) {
  return impl.toDuplex(
    ipcRenderer,
    ipcRenderer,
    impl.CLIENT_CHANNEL,
    impl.SERVER_CHANNEL,
  );
};
