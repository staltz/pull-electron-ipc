var EventEmitter = require('events');

const CLOSED_EVENT = 'pull-electron-ipc-close';

function createReorderedSource(channel) {
  var emitter = new EventEmitter();
  setTimeout(function () {
    emitter.emit(channel, 'READY');
  });
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 30, seq: 3});
  }, 20);
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 10, seq: 1});
  }, 40);
  setTimeout(function () {
    emitter.emit(channel, {type: 'end', seq: 4});
  }, 60);
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 20, seq: 2});
  }, 80);
  emitter.send = function (channel, raw) {
    emitter.emit(channel, raw);
  };
  setTimeout(function () {
    emitter.emit(CLOSED_EVENT, null);
  }, 100);
  return emitter;
}

function createSource(channel) {
  var emitter = new EventEmitter();
  setTimeout(function () {
    emitter.emit(channel, 'READY');
  });
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 10, seq: 1});
  }, 20);
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 20, seq: 2});
  }, 40);
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 30, seq: 3});
  }, 60);
  setTimeout(function () {
    emitter.emit(channel, {type: 'end', seq: 4});
  }, 80);
  setTimeout(function () {
    emitter.emit(CLOSED_EVENT, null);
  }, 100);
  emitter.send = function (channel, raw) {
    emitter.emit(channel, raw);
  };
  return emitter;
}

function createReorderedPort() {
  var channel = 'message';
  var emitter = new EventEmitter();
  setTimeout(function () {
    emitter.emit(channel, 'READY');
  });
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 30, seq: 3});
  }, 20);
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 10, seq: 1});
  }, 40);
  setTimeout(function () {
    emitter.emit(channel, {type: 'end', seq: 4});
  }, 60);
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 20, seq: 2});
  }, 80);
  setTimeout(function () {
    emitter.emit(CLOSED_EVENT, null);
  }, 100);
  emitter.postMessage = function (raw) {
    emitter.emit('message', raw);
  };
  return emitter;
}

function createPort() {
  var channel = 'message';
  var emitter = new EventEmitter();
  setTimeout(function () {
    emitter.emit(channel, 'READY');
  });
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 10, seq: 1});
  }, 20);
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 20, seq: 2});
  }, 40);
  setTimeout(function () {
    emitter.emit(channel, {type: 'data', data: 30, seq: 3});
  }, 60);
  setTimeout(function () {
    emitter.emit(channel, {type: 'end', seq: 4});
  }, 80);
  setTimeout(function () {
    emitter.emit(CLOSED_EVENT, null);
  }, 100);
  emitter.postMessage = function (raw) {
    emitter.emit('message', raw);
  };
  return emitter;
}

function createSink(cb) {
  var emitter = new EventEmitter();
  emitter.send = function (channel, raw) {
    if (raw === 'READY') {
      emitter.emit('pull-electron-ipc-renderer', 'READY');
      return;
    }
    cb(channel, raw);
  };
  return emitter;
}

function createTransform(channelHere) {
  var emitter = new EventEmitter();
  emitter.send = function (channelThere, raw) {
    if (raw === 'READY') {
      emitter.emit('pull-electron-ipc-renderer', 'READY');
      return;
    }
    var msg = raw;
    var x = msg.data;
    var y = x * 5;
    emitter.emit(channelHere, {type: msg.type, data: y, seq: msg.seq});
  };
  return emitter;
}

module.exports = {
  port: createPort,
  reorderedPort: createReorderedPort,
  source: createSource,
  reorderedSource: createReorderedSource,
  sink: createSink,
  transform: createTransform,
};
