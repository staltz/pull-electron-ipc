var impl = require('./implementation');

module.exports = function setup(worker, ipcMain, webContents) {
  worker.on('message', (msg) => {
    webContents.send(impl.CLIENT_CHANNEL, msg);
  });
  ipcMain.on(impl.SERVER_CHANNEL, (msg) => {
    worker.postMessage(msg);
  });
};
