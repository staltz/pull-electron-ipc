var impl = require('./implementation');

module.exports = function toDuplex(parentPort) {
  parentPort.send = function send(_, msg) {
    parentPort.postMessage(msg);
  };
  return impl.toDuplex(parentPort, parentPort, 'message', '');
};
