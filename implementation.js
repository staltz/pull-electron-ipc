const quickInsert = require('quick-insert');

const READY_EVENT = 'READY';
const CLOSED_EVENT = 'pull-electron-ipc-close';

function toDuplex(listenable, sendable, channelHere, channelThere) {
  let gotReady = false;
  let sinkRead;
  let handshakeInterval;
  const msgArr = [];
  const cbArr = [];
  let isReceiving = false;
  let outSeq = 0;
  let waitingForInSeq = 1;
  let isSending = false;
  let closed = false;

  function startHandshake() {
    handshakeInterval = setInterval(function tryHandshake() {
      if (closed) return;
      sendable.send(channelThere, READY_EVENT);
    }, 100);
  }

  function endHandshake() {
    if (gotReady) return;
    if (closed) return;
    gotReady = true;
    if (handshakeInterval) {
      clearInterval(handshakeInterval);
      handshakeInterval = null;
    }
    sendable.send(channelThere, READY_EVENT);
    if (sinkRead) {
      write(sinkRead);
      sinkRead = null;
    }
  }

  function endDuplex() {
    setTimeout(function tryToEndDuplex() {
      if (!isReceiving && !isSending) {
        if (listenable.removeListener) {
          listenable.removeListener(channelHere, onMsg);
        } else if (listenable.off) {
          listenable.off(channelHere, onMsg);
        }
      } else {
        setTimeout(tryToEndDuplex);
      }
    });
  }

  function consumeReads() {
    let msg;
    let cb;
    while (msgArr.length && cbArr.length && msgArr[0].seq === waitingForInSeq) {
      msg = msgArr.shift();
      cb = cbArr.shift();
      waitingForInSeq += 1;
      switch (msg.type) {
        case 'data':
          if (msg.format === 'buffer') {
            cb(null, Buffer.from(msg.data, 'base64'));
          } else {
            cb(null, msg.data);
          }
          break;
        case 'error':
          cb(msg.data);
          isReceiving = false;
          endDuplex();
          return;
        case 'end':
          cb(true);
          isReceiving = false;
          endDuplex();
          return;
        default:
          (console.warn || console.log)(
            'pull-electron-ipc cannot recognize message',
            msg,
          );
          break;
      }
    }
  }

  function onMsg(first, second) {
    const msg = second || first;
    if (msg === READY_EVENT) return endHandshake();
    if (msg === CLOSED_EVENT) return close();
    quickInsert(msg, msgArr, (m1, m2) => {
      if (m1.seq === m2.seq) return 0;
      return m1.seq < m2.seq ? -1 : 1;
    });
    consumeReads();
  }

  function read(abort, cb) {
    isReceiving = true;
    if (!cb) throw new Error('MUST provide cb to pull-electron-ipc source');
    if (abort) {
      while (cbArr.length) {
        cbArr.shift()(abort);
      }
      cb(abort);
      isReceiving = false;
      endDuplex();
    } else {
      cbArr.push(cb);
      consumeReads();
    }
  }

  function write(myRead) {
    if (closed) return;
    if (!gotReady) {
      sinkRead = myRead;
      return;
    }

    isSending = true;
    myRead(null, function next(end, data) {
      if (closed) return;
      outSeq += 1;
      if (end === true) {
        const msg = {type: 'end', seq: outSeq};
        sendable.send(channelThere, msg);
        isSending = false;
        endDuplex();
      } else if (end) {
        const msg = {type: 'error', data: end, seq: outSeq};
        sendable.send(channelThere, msg);
        isSending = false;
        endDuplex();
      } else {
        const send = Buffer.isBuffer(data) ? data.toString('base64') : data;
        const format = Buffer.isBuffer(data) ? 'buffer' : 'json';
        const msg = {type: 'data', format, data: send, seq: outSeq};
        sendable.send(channelThere, msg);
        myRead(null, next);
      }
    });
  }

  function close() {
    closed = true;
  }

  if (listenable.addListener) {
    listenable.addListener(channelHere, onMsg);
    listenable.addListener(CLOSED_EVENT, close);
  } else if (listenable.on) {
    listenable.on(channelHere, onMsg);
    listenable.on(CLOSED_EVENT, close);
  } else {
    throw new Error(
      channelHere + ' cannot call neither `on` nor `addListener`',
    );
  }

  startHandshake();

  return {
    source: read,
    sink: write,
  };
}

module.exports = {
  toDuplex: toDuplex,
  CLIENT_CHANNEL: 'pull-electron-ipc-renderer',
  SERVER_CHANNEL: 'pull-electron-ipc-main',
};
