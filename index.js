module.exports = function toDuplex() {
  throw new Error(
    "You're not supposed to import pull-electron-ipc directly. " +
      'Instead, ' +
      "require('pull-electron-ipc/worker') or " +
      "require('pull-electron-ipc/main') or " +
      "require('pull-electron-ipc/renderer')"
  );
};
